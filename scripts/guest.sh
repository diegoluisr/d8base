#!/bin/bash

# Run this command in guest only

apt-get update

apt-get install -y nano
apt-get install -y git

cd /var/www/html
composer require --no-plugins --no-scripts drush/drush

# drush self-update

composer require --no-plugins --no-scripts drupal/console:~1.0 --prefer-dist --optimize-autoloader
composer require --no-plugins --no-scripts wtfzdotnet/php-tmdb-api

# cd /root/scripts
# bash fix-permissions.sh --drupal_path=/var/www/html --drupal_user=www-data

# cp -f /var/www/config/starter/settings.php /var/www/html/sites/default/settings.php

# drush config-set "system.site" uuid "cd0aaf50-d88f-4ea1-8aac-932ce37b0a2f"
# drupal config:override system.site uuid cd0aaf50-d88f-4ea1-8aac-932ce37b0a2f
