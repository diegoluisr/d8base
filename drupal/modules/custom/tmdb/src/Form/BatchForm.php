<?php
namespace Drupal\tmdb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class BatchForm extends ConfigFormBase {

  public function getFormID() {
    return 'tmdb_batch_form';
  }

  public function getEditableConfigNames() {
    return [
      'tmdb_batch.settings'
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {}

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $token  = new \Tmdb\ApiToken('c672da4254c76191e8b23b654e0c59d8');
    $client = new \Tmdb\Client($token);

    $repository = new \Tmdb\Repository\MovieRepository($client);

    print_r($repository->getPopular());

    exit();
  }

}