<?php

$settings['hash_salt'] = file_get_contents('/var/www/config/salt');

$databases['default']['default'] = array (
  'database' => 'test',
  'username' => 'root',
  'password' => 'root',
  'prefix' => '',
  'host' => '192.168.99.100',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
$settings['install_profile'] = 'standard';
$config_directories['sync'] = 'sites/default/files/config_r5qUKAK3tv2okaUx7t-gr6EnN6zclFMQlpifAh8FQbhzxzq3uY8t27mn5zMGiQIOZDXmezZ22w/sync';
